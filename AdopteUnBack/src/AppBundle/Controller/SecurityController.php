<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use Symfony\Component\HttpFoundation\Response;

use AppBundle\Entity\User;
use AppBundle\Form\UserType;
use AppBundle\Form\UserSigninType;

use Symfony\Component\HttpFoundation\Session\Session;

use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\VarDumper\VarDumper;

class SecurityController extends Controller
{
    /**
     * @Route("/", name="redirect_login")
     */
    public function redirectToLoginAction(){
        return $this->redirectToRoute('signin');
    }

    /**
     * @Route("register", name="signup")
     */
    public function signupAction(Request $request, UserPasswordEncoderInterface $encoder) {
        $em = $this->getDoctrine()->getManager();
        $user = new User();

        $form = $this->createForm(UserType::class, $user);

        $form->handleRequest($request);
        if ($form->isValid() && $form->isSubmitted()) {
            $encoded = $encoder->encodePassword($user, $user->getPassword());
            $user->setPassword($encoded);
            $user->setUserName($user->getEmail());
            $user->setCreatedAt(new \Datetime());
            $user->setEnabled(false);
            $em->persist($user);
            $em->flush();

            return $this->redirectToRoute('signin');
        }

        $form = $form->createView();

        return $this->render('@App/Security/signup.html.twig', compact('form'));
    }

    /**
     * @Route("login", name="signin")
     */
    public function signinAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $user = new User();
        
        $form = $this->createForm(UserSigninType::class, $user);

        $form->handleRequest($request);
        if ($form->isValid() && $form->isSubmitted()) {
            $user = $em->getRepository('AppBundle:User')->findOneByEmail($user->getEmail());

            if($user == null) {
                $request->getSession()->getFlashBag()->add('danger', 'Adresse mail non trouvée');
                return $this->redirectToRoute('signin');
            }

            $encoderService = $this->container->get('security.password_encoder');

            if($encoderService->isPasswordValid($user, $form->getData()->getPassword())) {
                //Handle getting or creating the user entity likely with a posted form
                // The third parameter "main" can change according to the name of your firewall in security.yml
                $token = new UsernamePasswordToken($user, null, 'main', $user->getRoles());
                $this->get('security.token_storage')->setToken($token);

                // If the firewall name is not main, then the set value would be instead:
                // $this->get('session')->set('_security_XXXFIREWALLNAMEXXX', serialize($token));
                $this->get('session')->set('_security_main', serialize($token));
                
                // Fire the login event manually
                $event = new InteractiveLoginEvent($request, $token);
                $this->get("event_dispatcher")->dispatch("security.interactive_login", $event);
                if(in_array('ROLE_ADMIN', $user->getRoles())){
                    return $this->redirectToRoute('admin_homepage');
                }
                else {
                    //return $this->redirectToRoute('admin_homepage');
                    return $this->redirectToRoute('profil.edit');
                }
            }
            else 
                $request->getSession()->getFlashBag()->add('danger', 'Mauvais mot de passe');
        }
        $form = $form->createView();

        return $this->render('@App/Security/signup.html.twig', compact('form'));
    }
}